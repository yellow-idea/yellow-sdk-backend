"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.upperCaseFirstLetter = exports.snakeToCamel = void 0;

var snakeToCamel = function snakeToCamel(str) {
  return str.replace(/([-_][a-z])/g, function (group) {
    return group.toUpperCase().replace('-', '').replace('_', '');
  });
};

exports.snakeToCamel = snakeToCamel;

var upperCaseFirstLetter = function upperCaseFirstLetter(str) {
  return str[0].toUpperCase() + str.slice(1);
};

exports.upperCaseFirstLetter = upperCaseFirstLetter;