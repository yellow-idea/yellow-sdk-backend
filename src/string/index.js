export const snakeToCamel = str => str.replace(
    /([-_][a-z])/g ,
    ( group ) => group.toUpperCase()
        .replace('-' , '')
        .replace('_' , '')
)

export const upperCaseFirstLetter = str => str[ 0 ].toUpperCase() + str.slice(1)
