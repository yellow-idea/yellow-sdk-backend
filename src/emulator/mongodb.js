import mongoose from 'mongoose'

const EmulatorMongoDB = async ( host , callback ) => {
    mongoose.Promise = global.Promise
    mongoose.set('useCreateIndex' , true)
    try {
        await mongoose.connect(host , { useNewUrlParser : true , useUnifiedTopology : true })
        console.info('API Version 1 Connect Success')
        await callback()
    } catch ( e ) {
        console.error(e)
    }
}

export default EmulatorMongoDB
