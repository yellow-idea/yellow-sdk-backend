"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "EmulatorMongoDB", {
  enumerable: true,
  get: function get() {
    return _mongodb["default"];
  }
});

var _mongodb = _interopRequireDefault(require("./mongodb"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }