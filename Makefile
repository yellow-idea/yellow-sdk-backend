build: bd

bd:
	yarn build
	yarn build_string
	yarn build_emulator
	git add .
	git commit -m "$(title)"
	git push origin master
